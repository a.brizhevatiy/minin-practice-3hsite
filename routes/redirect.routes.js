const {Router} = require('express')
const Link = require("../models/Link")
const router = Router()

router.get('/:code', async (req, res) => {
    try {

        const link = await Link.findOne({code: req.params.code}) //  поллучаем объект

        if (link) { // проверка на наличие
            link.clicks++ // увеличиваем счетчик
            await link.save() // сохраняем измененные данные
            return res.redirect(link.from) // перенаправляем на внешнюю ссылку, которая лежит в параметре from
        }
        res.status(404).json("No such link.") // если не найдено

    } catch (e) {
        res.status(500).json({message: "Something goes wrong."})
    }
})


module.exports = router