const {Router} = require("express")
const bcrypt = require("bcryptjs") // модуль для хеширования паролей, так же позволяет их сравнивать
const jwt = require("jsonwebtoken") // модуль для работы с токеном
const config = require("config")
const {check, validationResult} = require("express-validator") // модуль для валидации данных
const User = require('../models/User')
const router = Router()

//Эндпоинты для дальнейшей работы

// prefix: /api/auth/register
router.post(
    '/register',
    [ // массив middleware'ов для валидации
        check('email', "Incorrect email!").isEmail(), //проверка поля email
        check("password", "Min pass length - 6 chars").isLength({min: 6}) // проверка пароля на мин кол-во символов

    ],
    async (req, res) => {
        try {

            const errors = validationResult(req); // в данном месте Экспресс-валидатор проверяет входящие поля

            if (!errors.isEmpty()) { // возврат ошибки
                return res.status(400).json({
                    errors: errors.array(),
                    message: "Incorrect registration data!"
                })
            }

            //отправляться будет два поля, которые получаем из запроса
            const {email, password} = req.body

            const candidate = await User.findOne({email})// используется для проверки наличия пользователя в БД
            if (candidate) {
                return res.status(400).json({message: "User already exist!"})
            }
            const hashedPassword = await bcrypt.hash(password, 12) // Шифруем пароль, вторым параметром идет Solt, асинхронная
            const user = new User({email, password: hashedPassword})

            const test = await user.save() // Ждем сохранения пользователя
            res.status(201).json({message: "New user created! "})

        } catch (e) {
            res.status(500).json({message: "Something goes wrong."})
        }
    })
// ----------------------------
// prefix: /api/auth/login
router.post(
    '/login',
    [ // массив middleware'ов для валидации
        check('email', "Incorrect email!").normalizeEmail().isEmail(), //проверка поля email
        check("password", "Enter password").exists() // проверка пароля на наличие символов
    ],

    async (req, res) => {
        try {
            const errors = validationResult(req); // в данном месте Экспресс-валидатор проверяет входящие поля
            if (!errors.isEmpty()) { // возврат ошибки
                return res.status(400).json({
                    errors: errors.array(),
                    message: "Incorrect login data!"
                })
            }
            const {email, password} = req.body; // получаем логин и пароль из запроса

            const user = await User.findOne({email}) // ищем пользователя

            if (!user) {
                return res.status(400).json({message: "No such user."}) // возврат ошибки если не найден
            }

            const isMatch = await bcrypt.compare(password, user.password) // сравниваем пароли
            if (!isMatch) {
                return res.status(400).json({message: "Wrong password."})
            }

            // Авторизация будет идти через jvt token

            const token = jwt.sign( // Создаем токен, передавая в него три параметра
                {userId: user.id}, // Первым параметром указываем параметры, которые будут зашифровны в данном токене
                config.get('jwtSecret'),// Вторым параметром передается секретный ключ, который берез из Конфига
                {expiresIn: "1h"}// Третьим параметром идет объект, в котором указывается срок жизни токена
            )

            res.json({token, userId: user.id})

        } catch (e) {
            res.status(500).json({message: "Something goes wrong."})
        }
    })

module.exports = router