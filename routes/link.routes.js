const {Router} = require('express')
const config = require('config')
const shortid = require('shortid') // библиотека для генерации коротких кодов
const Link = require('../models/Link') // работает со схемой БД
const auth = require('../middleware/auth.middleware') // подключаем Middleware и добавляем его в Endpoint
const router = Router()


//middleware auth используется, чтобы не авторизированные пользователи не могли создавать эти ссылки
router.post('/generate', auth, async (req, res) => {
    try {
        const baseUrl = config.get('baseUrl') // получаем базовый адрес приложения из файла конфига
        const {from} = req.body // в последствии нужно будет редиректить пользователя по данному пути

        const code = shortid.generate() // получение уникального кода, который указывает на нашу ссылку

        const existing = await Link.findOne({from}) // проверка на наличие в базе такой ссылки

        if (existing) { // если ччылка есть - возвращаем
            return res.json({link: existing})
        }

        //формируем сокращенную ссылку, которая будет работать в сервисе

        const to = baseUrl + '/t/' + code

        const link = new Link({ // создаем новый объект ссылки
            code, to, from, owner: req.user.userId // чтобы данный формат был доступен необходимо добавить auth
        })

        await link.save() // сохраняем ссылку

        res.status(201).json({link}) //возвращаем ссылку

    } catch (e) {
        res.status(500).json({message: "Something goes wrong."})
    }
})

router.get('/', auth, async (req, res) => {
    try {
        const links = await Link.find({owner: req.user.userId}) // получение ссылок текущего пользователя
        //req.user.userId берется благодаря расшифровке токена пользователя в middleware. Туда он попадает из auth.routes
        res.json(links)
    } catch (e) {
        res.status(500).json({message: "Something goes wrong."})
    }
})

router.get("/:id", auth, async (req, res) => {
    try {
        const link = await Link.findById(req.params.id) // получение олтдельной ссылки текущего пользователя
        res.json(link)
    } catch (e) {
        res.status(500).json({message: "Something goes wrong."})
    }
})


module.exports = router