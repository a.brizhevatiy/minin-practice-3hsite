const {Schema, model, Types} = require("mongoose")

//описание полей объекта:
//Тип - Строка, поле обязательно, значение уникально
// type: Types.objectId как связка модели пользователя и определенной записи в БД
const schema = new Schema({
    from: {type: String, required: true},
    to: {type: String, required: true, unique: true},
    code: {type: String, required: true, unique: true},
    date: {type: Date, default: Date.now},
    clicks: {type: Number, default: 0},
    owner: {type: Types.ObjectId, ref: 'User'}
})

module.exports = model('Link', schema)