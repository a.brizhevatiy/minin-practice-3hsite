const {Schema, model, Types} = require("mongoose")

//описание полей объекта:
//Тип - Строка, поле обязательно, значение уникально
// type: Types.objectId как связка модели пользователя и определенной записи в БД
const schema = new Schema({
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    links: [{type: Types.ObjectId, ref: "Link"}]
})

module.exports = model('User', schema)