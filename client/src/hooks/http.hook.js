import {useState, useCallback} from "react"

export const useHttp = () => {
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(null)
    const request = useCallback(async (url, method = 'GET', body = null, headers = {}) => {
        setLoading(true)
        try {
            if (body) {
                body = JSON.stringify(body) // приводим к JSON в случае, если передаем
                headers["Content-Type"] = 'application/json' // явно указываем в заголовке, что будет передаваться json
            }
            const response = await fetch(url, {method, body, headers}) // ожидаем ответ от сервера
            const data = await response.json() // преобразуем его в json

            if (!response.ok) { // выкидываем ошибку если пришла ошибка в запросе
                throw new Error(data.message || 'Something goes wrong.')
            }

            setLoading(false)
            return data

        } catch (e) { // обрабатываем ошибку
            setLoading(false)
            setError(e.message)
            throw e
        }
    }, [])

    const clearError = useCallback(() => setError(null), [])

    return {loading, request, error, clearError}
}