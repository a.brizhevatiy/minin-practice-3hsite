//работа с jvt токеном - когда мы его получаем, то храним в LocalStorage и при след запуске проверяем его наличие
import {useState, useCallback, useEffect} from "react"

const storageName = 'userData' // переменная для хранения данных в Storage

export const useAuth = () => {
    const [token, setToken] = useState(null)
    const [ready, setReady] = useState(false)
    const [userId, setUserId] = useState(null)

    const login = useCallback((jwtToken, id) => {  // в данном методе будет просто сохраняться токен
        setToken(jwtToken)
        setUserId(id)

        localStorage.setItem(storageName, JSON.stringify({
            userId: id, token: jwtToken
        })) // записываем данные в локальное хранилище

    }, [])


    const logout = useCallback(() => { // очищаем данные
        setToken(null)
        setUserId(null)
        localStorage.removeItem(storageName)

    }, [])


    useEffect(() => { // проверка на наличие записи в локальном хранилище
        const data = JSON.parse(localStorage.getItem(storageName))
        if (data && data.token) {
            login(data.token, data.userId)
        }
        setReady(true)
    }, [login])


    return {login, logout, token, userId, ready}
}