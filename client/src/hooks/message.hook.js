import {useCallback} from 'react'

export const useMessage = () => {
    return useCallback(text => {
        if (window.M && text) { // используется объект из Materialize который отображает всыплывающее окно
            window.M.toast({html: text}) // передаем в него текст
        }
    }, [])
}