import React from "react";
import {BrowserRouter as Router} from "react-router-dom"
import {useRoutes} from "./routes"
import {useAuth} from "./hooks/auth.hook";
import {Navbar} from "./components/Navbar";
import {Loader} from "./components/Loader";
import {AuthContext} from "./context/AuthContext";
// используется для доступа к контексту по всему приложению
// в value парадаются данные контекста
import "materialize-css"

function App() {
    const {token, logout, login, userId, ready} = useAuth() // получаем параметры и черех контекст они будут передаваться всему приложению
    const isAuthenticated = !!token
    const routes = useRoutes(isAuthenticated)

    if(!ready){
        return <Loader />
    }
    return (
        <AuthContext.Provider value={{token, logout, login, userId, isAuthenticated}}>
            <Router>
                {isAuthenticated && <Navbar />}
                <div className={"container"}>
                    {routes}
                </div>
            </Router>
        </AuthContext.Provider>
    );
}

export default App;
