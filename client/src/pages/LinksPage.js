import React, {useCallback, useContext, useEffect, useState} from 'react';
import {useHttp} from "../hooks/http.hook";
import {AuthContext} from "../context/AuthContext";
import {Loader} from "../components/Loader";
import {LinksList} from "../components/LinksList";
import {useHistory} from 'react-router-dom'

export const LinksPage = () => {
    const [links, setLinks] = useState()
    const {loading, request} = useHttp()
    const {token, isAuthenticated} = useContext(AuthContext)
    const history = useHistory();

    if(!isAuthenticated){
        history.push('/')
    }

    const fetchLinks = useCallback(async () => {
        try {
            const fetched = await request('/api/link', 'GET', null, {
                Authorization: `Bearer ${token}`
            })
            setLinks(fetched)
        } catch (e) {
        }
    }, [token, request])

    useEffect(() => {
        fetchLinks()
    }, [fetchLinks])

    if (loading) {
        return <Loader />
    }

    return (
        <>
            {!loading && <LinksList links={links}/>}
        </>
    );
};

