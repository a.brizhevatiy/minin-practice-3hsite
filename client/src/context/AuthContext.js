import {createContext} from"react"// контекст создается через обычную функцию в Реакте - createContext
// он может пердавать параметры не по древовидной структре, а по всему приложению
function noop (){} // функция заглушка

export const AuthContext = createContext({ // создаем контекст
    token: null,
    userId: null,
    login: noop,
    logout: noop,
    isAuthenticated: false
})