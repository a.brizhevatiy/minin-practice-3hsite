const express = require('express')
const config = require("config")
const path = require('path')
const mongoose = require("mongoose") // Пакет для работы с МонгоДБ

const app = express()

app.use(express.json({extended: true})) // Добавляетс для корректного парса Body в JSON

app.use('/api/auth', require('./routes/auth.routes')) // подключаем описание роутов
app.use('/api/link', require('./routes/link.routes'))
app.use('/t', require('./routes/redirect.routes'))

if (process.env.NODE_ENV === 'production') {
    app.use('/', express.static(path.join(__dirname, 'client', 'build')))

    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
    })
}

const PORT = config.get('port') || 5000 // Данные порта берутся из файла конфига впапке config, использовался пакет config

async function start() {
    try {
        await mongoose.connect(config.get('mongoUri'), { //Асинхронная функция, которая возвращает промис, используеься для подключения к МонгоДБ
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        })
        app.listen(PORT, () => console.log(`App has been started on port ${PORT}...`)) // Вешаем Слушателя на порт
    } catch (e) {
        console.log('Server Error', e.message)
        process.exit(1)
    }

}

start()


